var canvas = oCanvas.create({
    canvas: "#canvas",
    background: "#000" //image("/EngageME/static/images/classlayout01.gif")
});
var deskWidth  = 60;
var deskHeight = 45;

var numStudents = {{=numStudents}};
var numVideos   = {{=numVideos}};
var studentName = "Boo Shanks";

function createStudent( canvas, _x, _y, _w, _h ) {
    return canvas.display.image({
        x: _x,
        y: _y,
        width: _w,
        height: _h,
        origin: { x: "center", y: "center" },
        image: "/EngageMe/static/images/desk.jpg"
    });
}

var desk      = createStudent(canvas,  30, 23, deskWidth, deskHeight);
var deskTwo   = createStudent(canvas, 100, 23, deskWidth, deskHeight);
var deskThree = createStudent(canvas, 170, 23, deskWidth, deskHeight);

var text = canvas.display.text({
    x: 177,
    y: 107,
    origin: { x: "center", y: "top" },
    font: "bold 12px sans-serif",
    text: studentName,
    fill: "#0aa"
});

var dragOptions = { changeZindex: true };

desk.dragAndDrop(dragOptions);
deskTwo.dragAndDrop(dragOptions);
deskThree.dragAndDrop(dragOptions);
text.dragAndDrop(dragOptions);

canvas.addChild(desk);
canvas.addChild(deskTwo);
canvas.addChild(deskThree);
canvas.addChild(text);