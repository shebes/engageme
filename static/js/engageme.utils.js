var singleStudentPieChart;
var viewingSingleStudent = 0;
var previousNgmntNdx = -1;
var TIME_ADJUST      = 60 * 5;
var classEngagement  = new Array();
var lastSecond = 0;
/*==================================================================================*
  The following functions can be placed in a javascript library, they are not 
  specific to this page alone and are already abstract enough to move.
 *==================================================================================*/
function hideComponent(id) { $('#'+id).hide(); }
function showComponent(id) { $('#'+id).show(); }
function changeStudentColorRed(target)
{ $('#'+target).css({"background":"red","color":"white"}); }
function changeStudentColorYellow(target)
{ $('#'+target).css({"background":"yellow","color":"black"}); }
function changeStudentColorGreen(target)
{ $('#'+target).css({"background":"green","color":"white"}); }
// change color of student button
function changeStudentColor( level, target )
{
    if ( level.localeCompare('high') == 0 )
        changeStudentColorGreen(target);
    else if ( level.localeCompare('medium') == 0 )
        changeStudentColorYellow(target);
    else
        changeStudentColorRed(target);
}

function getDate(d) {
	var dt = new Date(d.date);;
	return dt;
}

function graph_showData(obj, d) {
	var coord = d3.mouse(obj);
	var infobox = d3.select(".infobox");
	// now we just position the infobox roughly where our mouse is
	//infobox.style("left", (coord[0] + 100) + "px" );
	//infobox.style("top", (coord[1] - 175) + "px");
	infobox.style("left", (coord[0] + 200) + "px" );
	infobox.style("top", (coord[1] - 100) + "px");
	$(".infobox").html(d);
	$(".infobox").show();
}

function graph_hideData() { $(".infobox").hide(); }


var drawChart = function(data) {
	// define dimensions of graph
	//var m = [20, 40, 20, 100]; // margins
	var m = [20, 0, 20, 50]; // margins
	var w = 1100;//700 - m[1] - m[3]; // width
	var h = 200 - m[0] - m[2];//360 - m[0] - m[2]; // height
	data.sort(function(a, b) {
        var d1 = getDate(a);
		var d2 = getDate(b);
		if (d1 == d2) return 0;
		if (d1 > d2) return 1;
		return -1;
	});

	// get max and min dates - this assumes data is sorted
	var minDate = getDate(data[0]),
	maxDate = getDate(data[data.length-1]);

    alert('Min: ' + minDate.toLocaleTimeString() + '\nMax: ' + maxDate.toLocaleTimeString() );
	var x = d3.time.scale().domain([minDate, maxDate]).range([0, w]);

	// X scale will fit all values from data[] within pixels 0-w
	//var x = d3.scale.linear().domain([0, data.length]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
    var y = d3.scale.linear().domain([0, d3.max(data, function(d) { return d.eda; } )]).range([h, 0]);

	// create a line function that can convert data[] into x and y points
	var line = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// return the X coordinate where we want to plot this datapoint
		return x(getDate(d)); //x(i);
	})
	.y(function(d) {
		// return the Y coordinate where we want to plot this datapoint
		return y(d.eda);
	});
	function xx(e) { return x(getDate(e)); };
	function yy(e) { return y(e.eda); };

	//$("#chart").append("<p><small><em>Please move the mouse over data points to see details.</em></small></p>");

	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#chart").append("svg:svg")
		.attr("width", w + m[1] + m[3] )
		.attr("height", h + m[0] + m[2])
		.append("svg:g")
		.attr("transform", "translate(" + m[3] + "," + m[0] + ")");

	// create yAxis
	var xAxis = d3.svg.axis().scale(x).ticks(d3.time.minutes, 5).tickSize(-h).tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + h + ")")
		.call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y).ticks(10).orient("left");
	// Add the y-axis to the left
	graph.append("svg:g")
		.attr("class", "y axis")
		//.attr("transform", "translate(-25,0)")
		.call(yAxisLeft);

	// Add the line by appending an svg:path element with the data line we created above
	// do this AFTER the axes above so that the line is above the tick-lines
	/*graph
		.selectAll("circle")
		.data(data)
		.enter().append("circle")
		.attr("fill", "steelblue")
		.attr("r", 0.5)
		.attr("cx", xx)
		.attr("cy", yy)
		.on("mouseover", function(d) { graph_showData(this, d.eda);})
		.on("mouseout", function(){ graph_hideData();});
	*/

	graph.append("svg:path").attr("d", line(data));
	graph.append("svg:text")
		.attr("x", -135) // was -200
		.attr("y", -40) //was -90
		.attr("dy", ".2em")
		.attr("transform", "rotate(-90)")
		.text("Electrodermal Activity");
		//.text("Trending Value");


	$("#chart").append("<div class='infobox' style='display:none;'>Test</div>");
}

function changeEngagementValues( myPlayer )
{
    // if the timer moves by a second change the value line on the graph
    //var currentSecond = Math.floor( myPlayer.currentTime() );	
    var ngmntNdx   = Math.floor( myPlayer.currentTime() / TIME_ADJUST );
    //showComponent('student30');        
    if ( ngmntNdx != previousNgmntNdx ) 
    {
        previousNgmntNdx = ngmntNdx;
        
        loadClassEngagementAt( ngmntNdx );
        /*
        if ( detailChart != 0 )
        {
            alert(" What: " + detailChart.xAxis.length );
            //detailChart.xAxis[0].removePlotLine('movingline');
            detailChart.xAxis[0].addPlotLine(
                {
                    id:'movingline',
                    color:'#FF0000',
                    width:2,
                    value:currentSecond
                }
            );
        }
        */
    }
}

/*****************************************************************************
 * Highcharts Graphing
 * Description: 
 * Parameters: 
 * [+] element - 
 * [+] theList - 
 * Returns:
 * [+] 
 *****************************************************************************/
function createOptionsFromList(element,theList)
{ 
    // Removes all options for the select box
    $('#' + element + ' option').remove();
    $('#' + element).append("<option value=''>Select...</option>");
    // .each loops through the array
    $.each(theList, function(i){
        $('#' + element).append($("<option></option>")
                        .attr("value", theList[i]['value'])
                        .text(theList[i]['text']));
    });
}
/*****************************************************************************
 * Highcharts Graphing
 * Description: Build a table that shows the time window values for a note.
 *              This table is created and added to a popup note dialog window.
 * Parameters: 
 * [+] beginDate - start time for interest selection window
 * [+] endDate   - end   time for interest selection window
 * Returns:
 * [+] table as html script to be appended into a specific div
 *****************************************************************************/
function buildTimeTable(beginDate,endDate) {
    var output = "";
    output    += "<table class='ui-widget ui-widget-content'>";
    output    +=     "<tr class='ui-widget-header'>";
    output    +=        "<th class='ui-dialog-label control-label'>Start</th>";
    output    +=        "<td>" + beginDate.toDateString()       + "</td>";
    output    +=        "<td>" + beginDate.toLocaleTimeString() + "</td>";
    output    +=    "</tr>";
    output    +=    "<br/>";
    output    +=    "<tr class='ui-widget-header'>";
    output    +=        "<th class='ui-dialog-label control-label'>End</th>";
    output    +=        "<td>" + endDate.toDateString()       + "</td>";
    output    +=        "<td>" + endDate.toLocaleTimeString() + "</td>";
    output    +=    "</tr>";
    output    += "</table>";
    output    += "<br/>";
    return output;
}
/*****************************************************************************
 * Highcharts Graphing
 * Description: Create an engagement pie chart for a single student 
 * Parameters: 
 * [+] myContainer - name of the container in which to place the chart
 * [+] chartData   - the data to comprise the chart
 * Returns:
 * [+] A pie chart to display
 *****************************************************************************/
function createSingleStudentPieChart(myContainer, chartData) {
    singleStudentPieChart = $(myContainer).highcharts({
    //var pieChart = $(myContainer).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Student Engagement Over the Class Period'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format:  '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Engagement',
            data: chartData
        }]
    }).highcharts(); // return chart
    
}
 
/*****************************************************************************
 * Highcharts Graphing
 * Description: 
 * Parameters: 
 * [+] value - a valence option
 * Returns:
 * [+] Add a bunch of emotions to a drop down in a note dialog.
 *****************************************************************************/
function displayAndPopulateEmotion(value) {
    if ( value.localeCompare('high positive interest') == 0 ) {
        createOptionsFromList('emotion', hiArousalPos);
    } else if ( value.localeCompare('low positive interest') == 0 ) {
        createOptionsFromList('emotion', loArousalPos);
    } else if ( value.localeCompare('high negative interest') == 0 ) {
        createOptionsFromList('emotion', hiArousalNeg);
    } else if ( value.localeCompare('low negative interest') == 0 ) {
        createOptionsFromList('emotion', loArousalNeg);
    }
    $('#emotion-div').show();
}
/**/

var detailChart = 0;
var numMasks  = 1;
var beginDate = new Date();
var endDate   = new Date();
var valenceList  = [{'value':'high positive interest','text':'Hi Positive Interest'},
                    {'value':'low positive interest', 'text':'Lo Positive Interest'},
                    {'value':'high negative interest','text':'Hi Negative Interest'},
                    {'value':'low negative interest', 'text':'Lo Negative Interest'}];
var hiArousalPos = [{'value':'ecstasy','text':'ecstasy'},
                    {'value':'euphoria','text':'euphoria'},
                    {'value':'excitement','text':'excitement'},
                    {'value':'happiness','text':'happiness'},
                    {'value':'pleasure','text':'pleasure'},
                    {'value':'surprise','text':'surprise'},
                    {'value':'wonder','text':'wonder'},
                    {'value':'zeal','text':'zeal'},
                    {'value':'zest','text':'zest'},
                    {'value':'astonishment','text':'astonishment'},
                    {'value':'delighted','text':'delighted'}];
var loArousalPos = [{'value':'contentment','text':'contentment'},
                    {'value':'serenity','text':'serenity'},
                    {'value':'calm','text':'calm'},
                    {'value':'relaxed','text':'relaxed'},
                    {'value':'sleepy','text':'sleepy'}];
var hiArousalNeg = [{'value':'anger','text':'anger'},
                    {'value':'annoyance','text':'annoyance'},
                    {'value':'despair','text':'despair'},
                    {'value':'disgust','text':'disgust'},
                    {'value':'dread','text':'dread'},
                    {'value':'fear','text':'fear'},
                    {'value':'frustration','text':'frustration'},
                    {'value':'terror','text':'terror'},
                    {'value':'alarm','text':'alarm'}];
var loArousalNeg = [{'value':'boredom','text':'boredom'},
                    {'value':'depression','text':'depression'},
                    {'value':'misery','text':'misery'},
                    {'value':'sadness','text':'sadness'},
                    {'value':'shame','text':'shame'},
                    {'value':'sorrow','text':'sorrow'},
                    {'value':'suffering','text':'suffering'},
                    {'value':'worry','text':'worry'},
                    {'value':'tired','text':'tired'}];
                    

// create the detail chart
function createDetail(data) {

    if ( data.length == 0 )
        alert( "No data." );
    //alert( "Size of data: " + data.length );
    // prepare the detail chart
    var detailDate = new Array();
    var detailData = new Array();
    detailData = data;
    //var detailData = data, // [],
    //    detailStart = Date.UTC(2006,0,1);
    //var detailStart = data[0]['date'];
    //var detailStart = data[0][0];
    // put data in correct variables
    //$.each(data, function(i){
    //    detailData[i] = data[i]['eda'];
    //    detailDate[i] = new Date(data[i]['date']);
    //});
    //alert( "Date: " + detailDate[0] + "\tEDA: " + detailData[0] );


    // create a detail chart referenced by a global variable
    detailChart = $('#detail-container').highcharts({
        chart: {
            marginBottom: 120,
            reflow: false,
            marginLeft: 40,
            marginRight: 20,
            style: { position: 'absolute' },
            zoomType: 'x',
            events: {
                // listen to the selection event on the master chart to update the
                // extremes of the detail chart
                selection: function(event) {
                    event.preventDefault();
                    var xAxis = this.xAxis[0];
                    //beginDate = event.xAxis[0].min;
                    //endDate   = event.xAxis[0].max;
                    beginDate.setTime( event.xAxis[0].min );
                    endDate.setTime(   event.xAxis[0].max );
                    //alert( "min: " + beginDate.toLocaleString() + "\nmax: " + endDate.toLocaleString() );
                    // Prepare to open the note dialog
                    $( '#emotion-div' ).hide();
                    $( '#note-div'    ).hide();
                    $( '#time-div'    ).empty();
                    
                    // Create the valence options list in the note dialog
                    createOptionsFromList('valence',valenceList);
                    // Append/Create the start and end time display for the note dialog
                    $( '#time-div' ).append( buildTimeTable(beginDate,endDate) );
                    // Open the note dialog
                    $( '#dialog-form' ).dialog( 'open' );
                    
                    return false;
                }
            }  
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Student Interest Level during Class'
        },
        subtitle: {
            text: 'Select an area by dragging across the lower chart'
        },
        xAxis: {
            type: 'datetime',
            labels: {
                //format: '{value: %l:%M}',
                formatter: function() {
                    var realDate  = new Date(); 
                    var dateValue = Math.abs( this.value - (5*3600000) );
                    realDate.setTime(dateValue);
                    var hours = realDate.getHours();
                    //return hours + ':' + realDate.getMinutes();
                    return Highcharts.dateFormat('%l:%M', realDate);
                }
            }
        },
        yAxis: {
            title: {
                text: 'Electrodermal Activity'
            },
            maxZoom: 0.1
        },
        tooltip: {
            formatter: function() {
                var point     = this.points[0];
                var realDate  = new Date(); 
                var dateValue = Math.abs( this.x - (5*3600000) );
                realDate.setTime(dateValue);
                return '<b>' + point.series.name + '</b><br/>' +
                       Highcharts.dateFormat('%b.%e, %Y %l:%M', realDate) +
                       '<br/>EDA: ' + point.y;
            },
            shared: true
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 3
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Student Interest',
            data: detailData
        }],

        exporting: {
            enabled: false
        }              

    }).highcharts(); // return chart
	viewingSingleStudent = 1;
}

/* jQuery opens a dialog form when a chart section is selected. */
$(function () {
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 450,
		width: 400,
		modal: true,
		buttons: {
			"Save Note": function() {
			    //allFields.val( "" ).addClass("btn btn-primary");
				var xAxis    = detailChart.xAxis[0];
				var maskName = 'event-' + numMasks;
				numMasks += 1;
				// this should add a label to the selected area
				xAxis.addPlotBand({
					id: maskName,
					from: Date.UTC( beginDate.getFullYear(), 
									beginDate.getMonth(), 
									beginDate.getDate() ),
					to: Date.UTC(   endDate.getFullYear(), 
									endDate.getMonth(), 
									endDate.getDate() ),
					color: 'rgba(0, 100, 250, 0.4)',
					label: {
						text: maskName
					},
					events: {
						click: function (e) {
							xAxis.removePlotBand(this.id);
							$( '#dialog-form' ).dialog( 'open' );
							$( '#emotion-div' ).hide();
							$( '#note-div'    ).hide();
						}
					}
				});
				var addThis = '<h3>Valence: '  + valence.value 
							+ '<br/>Emotion: ' + emotion.value 
							+ '<br/>Note: '    + note.value 
							+ '</h3>';
				$('#selected-values').append(addThis);
				$(this).dialog("close");
			},
			Cancel: function() {
			    //allFields.val("").addClass("btn btn-primary");
				$(this).dialog("close");
			}
		},
		close: function() {
		    //allFields.val("").addClass("btn btn-primary");
			$(this).dialog("close");
		}
	});
});