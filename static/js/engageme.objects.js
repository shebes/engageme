/***********************************************************************
 * MyEngagement - beginning of object
 ***********************************************************************/
function MyEngagement_toString()
{
    var result = "[";
    result += "PID: " + this.participantid;

    result += ", Begin: " + this.begindatetime;//.toLocaleString();
    result += ", Duration: " + this.timestep;
    result += ", Num levels: " + this.levels.length;
    result += ", {";
    if ( this.levels.length > 0 )
    {
        for ( var i = 0 ; i < this.levels.length ; i++ )
        {
            if ( i > 0 ) { result += ", "; }
            result += this.levels[i];
        }
    }
    result += "}";
    result += "]";
    return result;
}

function MyEngagement_addLevel(level)
{
    this.levels.push(level);
}

function MyEngagement_getLevel(ndx)
{
    return this.levels[ndx];
}

function MyEngagement( participantid, begindatetime, duration )
{
    var that           = this;
    that.participantid = participantid;
    that.begindatetime = begindatetime;
    that.timestep      = duration;
    that.levels        = [];
    that.toString      = MyEngagement_toString;
    that.addLevel      = MyEngagement_addLevel;
    that.getLevel      = MyEngagement_getLevel;
}
/***********************************************************************
 * MyEngagement - end of object
 ***********************************************************************/