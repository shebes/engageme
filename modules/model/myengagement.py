#!/usr/bin/env python
# coding: utf8

import datetime

################################################################################
# Class that is used on the full class or group page.
class MyEngagement:

    # individual whose engagement levels are being stored
    _participantID = ''
    # the day the recording was made
    # the time the device started recording at the beginning of a task/class
    # we make this a datetime object
    _beginDateTime = ''
    # how much time a single level represents
    _step = 0
    # List of engagement level over time
    _levels = []

    ###
    # Constructor
    # Parameters:
    # [+] pid - participant identification
    # [+] beginDateTime - the beginning date and time of the engagement measurement
    # [+] step - number of minutes over which engagement window is considered
    ###
    def __init__( self, pid, beginDateTime, step ):
        self.setParticipant(   pid           )
        self.setBeginDateTime( beginDateTime )
        self.setTimestep(      step          )
        self._levels = []
    # end __init__

    def __str__( self ):
        result  = "[PID: %s, Begin: %s, Duration: %s, Num Levels: %d]"
        return result % (self._participantID, self._beginDateTime, self._step, len(self._levels) )
    # end toString

    def addLevel( self, level ): self._levels.append(level)
    # end addLevel
    def getLevel( self, ndx ): self._levels[ndx]
    # end getLevel


    ###############
    # SETTERS
    ###############
    def setParticipant(   self, value ): self._participantID = value # end setParticipant
    def setTimestep(      self, value ): self._step          = value # end setTimestep
    def setBeginDateTime( self, value ): 
        self._beginDateTime = value
        #dtFormat = '%Y-%m-%d %H:%M:%S'
        #self._beginDateTime = datetime.datetime.strptime( value, dtFormat )
    # end setBeginDateTime

    ###############
    # GETTERS
    ###############
    def getParticipant(self):   return self._participantID # end getParticipant
    def getBeginDateTime(self): return self._beginDateTime # end getBeginDateTime
    def getTimestep(self):      return self._step          # end getTimestep
    def getLevels(self):        return self._levels        # end getLevels
# end class MyEngagement
################################################################################
