#!/usr/bin/env python
# coding: utf8
from gluon import *

class TheClass:

    # Member Variables
    _subject    = ""
    _id         = ""
    _teacher    = ""
    _gradeLevel = ""
    _year       = ""

    #Constructor
    def __init__(self, id, teacher, subject, gradeLevel, year):
        self.init(id, teacher, subject, gradeLevel, year)
    #end constructor

    def init(self, id, teacher, subject, gradeLevel, year):
        self.setID(id)
        self.setTeacher(teacher)
        self.setSubject(subject)
        self.setGradeLevel(gradeLevel)
        self.setYear(year)
    #end init

    def __str__(self):
        result = "[%s (%s taught by %s for %s in %s)]"
        params = ( self.getID(), self.getSubject(), self.getTeacher(), self.getGradeLevel(), self.getYear() )
        # [ID (SUBJECT taught by TEACHER for GRADE LEVEL in YEAR)]
        return result % params

    #SETTERS
    def setID(self, value):         self._id         = value #end setID
    def setTeacher(self, value):    self._teacher    = value #end setTeacher
    def setSubject(self, value):    self._subject    = value #end setSubject
    def setGradeLevel(self, value): self._gradeLevel = value #end setGradeLevel
    def setYear(self, value):       self._year       = value #end setYear

    #GETTERS
    def getID(self):         return self._id         #end getID
    def getTeacher(self):    return self._teacher    #end getTeacher
    def getSubject(self):    return self._subject    #end getSubject
    def getGradeLevel(self): return self._gradeLevel #end getGradeLevel
    def getYear(self):       return self._year       #end getYear
    def getName(self):       return "%s %s" % (self._id, self._subject)
