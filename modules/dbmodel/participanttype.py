#!/usr/bin/env python
# coding: utf8
from gluon import *

class ParticipantType:
    _type = ""
    _description = ""

    def __init__(self, type, desc):
        init(type, desc)
    #end constructor

    def __str__(self):
        result  = "ParticipantType ( %s, %s )"
        params  = (self._type, self._description)
        return result % params

    # SETTERS
    def setType(self, value):        self._type        = value #end setType
    def setDescription(self, value): self._description = value #end setDescription

    # GETTERS
    def getType(self):        return self._type        #end getType
    def getDescription(self): return self._description #end getDescription
