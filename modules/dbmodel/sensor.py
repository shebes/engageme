#!/usr/bin/env python
# coding: utf8
from gluon import *

# sensor object
class Sensor:
    # unique identifier for sensor
    _id   = ""
    # type of sensor (e.g. GSR, video, EEG)
    _myType = ""

    def __init__( self, id, type ):
        setID(id)
        setType(type)
    #end constructor

    def __str__(self):
        result = "[Sensor (id: %s, type: %s)]"
        return result % ( self._id, self._type )
    #end toString

    #SETTERS
    def setID(self, value):   self._id   = value #end setID
    def setType(self, value): self._myType = value #end setType

    #GETTERS
    def getID(self):   return self._id   #end getID
    def getType(self): return self._myType #end getType
