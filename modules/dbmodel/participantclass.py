#!/usr/bin/env python
# coding: utf8
from gluon import *

class ParticpantClass:

    _participantID = ""
    _classID       = ""

    def __init__(self, participantID, classID):
        init(participantID, classID)
    # end constructor

    def init(self, pID, cID):
        setParticipantID(pID)
        setClassID(cID)
    # end init

    def __str__(self):
        result  = "ParticipantClass ( %s, %s )"
        return result % ( self._participantID, self._classID )
    #end toString

    # SETTERS
    def setParticipantID(self, value): self._participantID = value
    def setClassID(self, value):       self._classID       = value

    # GETTERS
    def getParticipantID(self): return self._participantID
    def getClassID(self):       return self._classID
