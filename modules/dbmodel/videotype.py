#!/usr/bin/env python
# coding: utf8
from gluon import *

class VideoType:
    _type = ""
    _desc = ""

    def __init__(self, type, desc):
        self.setType(type)
        self.setDesc(desc)
    #end constructor

    def __str__(self):
        result = "[VideoType (%s, %s)]"
        params = (self._type, self._desc)
        return result % params

    #SETTERS
    def setType(self, value): self._type = value #end setType
    def setDesc(self, value): self._desc = value #end setDesc
`
    #GETTERS
    def getType(self): return self._type #end getType
    def getDesc(self): return self._desc #end getDesc
