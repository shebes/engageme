#!/usr/bin/env python
# coding: utf8
from gluon import *

class ParticipantVideo:

    # Member variables
    _participantID = ""
    _classID = ""
    _location = ""
    _beginTime = ""
    _endTime   = ""

    # Class Constructor
    def __init__(self, pid, cid, location, begin, end):
        init( pid, cid, location, begin, end )
    #end constructor

    def init(self, pid, cid, location, begin, end):
        setParticipantID(pid)
        setClassID(cid)
        setLocation(location)
        setBeginTime(begin)
        setEndTime(end)
    #end initialization

    def __str__(self):
        result = "[ParticipantVideo (%s, %s, %s, %s, %s)]"
        params = (self._participantID, self._classID, self._location, self._beginTime, self._endTime)
    #end toString

    #SETTERS
    def setParticipantID(self, value): self._participantID = value #end setParticipantID
    def setClassID(self, value):       self._setClassID    = value #end setClassID
    def setLocation(self, value):      self.location       = value #end location
    def setBeginTime(self, value):     self._beginTime     = value #end setBeginTime
    def setEndTime(self, value):       self._endTime       = value #end setEndTime

    #GETTERS
    def getParticipantID(self): return self._participantID #end getParticipantID
    def getClassID(self):       return self._setClassID    #end getClassID
    def getLocation(self):      return self.location       #end getLocation
    def getBeginTime(self):     return self._beginTime     #getBeginTime
    def getEndTime(self):       return self._endTime       #end getEndTime
