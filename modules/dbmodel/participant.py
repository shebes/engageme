#!/usr/bin/env python
# coding: utf8

# Author: Shelby Solomon Darnell
# Creation Date: February 4, 2014
# Description:
#     Object to contain the elements of an entry from
#     the Participant table.
#______________________________________________________
class Participant:
    _ID        = "";
    _FirstName = "";
    _LastName  = "";
    _OtherNames= "";
    _Type      = "";

    def __init__(self, id, firstname, lastname, othernames, pType):
        self.setID(id)
        self.setType(pType)
        self.setFirstName(firstname)
        if ( othernames != "nmn" ):
            self.setOtherNames(othernames)
        self.setLastName(lastname)
    #end constructor

    def setID(self, id): self._ID = id #end setID
    def getID(self): return self._ID   #end getID

    def setType(self, value): self._Type = value #end setType
    def getType(self): return self._Type   #end getType

    def setFirstName(self, value): self._FirstName = value #end setFirstName
    def getFirstName(self): return self._FirstName         #end getFirstName

    def setOtherNames(self, value): self._OtherNames = value #end setOtherNames
    def getOtherNames(self): return self._OtherNames         #end getOtherNames

    def setLastName(self, value): self._LastName = value #end setLastName
    def getLastName(self): return self._LastName         #end getLastName

    def getFullName(self): return "%s %s %s" % ( self._FirstName, self._OtherNames, self._LastName )
    def getPartialName(self): return "%s %s%s" % ( self._FirstName[0:6], self._OtherNames[0], self._LastName[0] )

    # ToString method
    def __str__(self):
        out  = "Participant: %s (%s: %s %s %s)"
        out += "\n"
        return out % ( self._ID, self._Type, self._FirstName, self._OtherNames, self._LastName )
    # end toString
