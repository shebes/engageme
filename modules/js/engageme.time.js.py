
var serverTime = new Date();
function updateTime() {
    /// Increment serverTime by 1 second and update the html for '#time'
    serverTime = new Date(serverTime.getTime() + 1000);
    //$('#thatTime').html(serverTime.toLocaleString());
    $('#thatTime').html(formatThatTime(serverTime));
}
function dayToString(theDay)
{
    result = '';
    switch(theDay) {
        case 1:  result = 'Mon'; break;
        case 2:  result = 'Tue'; break;
        case 3:  result = 'Wed'; break;
        case 4:  result = 'Thu'; break;
        case 5:  result = 'Fri'; break;
        case 6:  result = 'Sat'; break;
        default: result = 'Sun'; break;
    }
    return result;
}
function monthToString(theMonth)
{
    result = '';
    switch(theMonth) {
        case  1: result = 'Feb'; break;
        case  2: result = 'Mar'; break;
        case  3: result = 'Apr'; break;
        case  4: result = 'May'; break;
        case  5: result = 'Jun'; break;
        case  6: result = 'Jul'; break;
        case  7: result = 'Aug'; break;
        case  8: result = 'Sep'; break;
        case  9: result = 'Oct'; break;
        case 10: result = 'Nov'; break;
        case 11: result = 'Dec'; break;
        default: result = 'Jan'; break;
    }
    return result;
}
function hourToString(theHour)
{
    result = theHour.toString();
    if ( theHour == 0 ) { result = '12'; }
    if ( theHour > 12 ) { result = (theHour - 12).toString(); }
    if ( theHour < 10 ) { result = '0' + result;}
    return result;
}
function formatThatTime(thatDT)
{
    result  = dayToString(thatDT.getDay());
    result += ' ';
    result += monthToString(thatDT.getMonth());
    result += ' ';
    result += thatDT.getDate().toString();
    result += ', ';
    result += thatDT.getFullYear();
    result += ' ';
    result += hourToString(thatDT.getHours());
    result += ':';
    result += thatDT.getMinutes();
    result += ':';
    result += thatDT.getSeconds();
    result += ' ';
    result += (thatDT.getHours()<12)?'AM':'PM';
    return result;
}

$(function() {
    updateTime();
    setInterval(updateTime, 1000);
});