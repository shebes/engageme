######################################################################
# Name: dbgetters.py
# Creator: Shelby Solomon Darnell
# Date: February 3, 2014 -- 0134 hours
#
# Description:
#     This class contains the database methods from pedometer.py in
#     the previous EngageME implementation.
#
######################################################################
import os
import sys
import datetime

from gluon.custom_import import track_changes; track_changes(True)
from dbmodel.participant import Participant
from dbmodel.TheClass    import TheClass


class DBGetters:
    def __init__(self):
        self.db = MyDB().getDB()
        self.debug = 0
    #end constructor

    #----------------------------------------------------------------#

    #**************************
    # Description:
    # Parameters:
    # [+] participantID - identification for participant
    # Returns: entry from participant table
    #**************************
    def getParticipant(self, participantID):
        fname = "getParticipant"
        query = "SELECT `p`.`id`, `p`.`firstname`, `p`.`lastname`, "
        query += "`p`.`othernames`, `p`.`typeid` "
        query += "FROM `participant` as `p` "
        query += "WHERE `p`.`id`='%s'"
        params = participantID
        dbResult = self.db.executesql( query % params )
        result = dbResult[0]
        myPart = Participant( result[0], result[1], result[2], result[3], result[4] )
        #myPart = ( result[0], result[1], result[2], result[3], result[4] )
        #d, firstname, lastname, othernames, type
        return myPart
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#
    def getTheClass(self, subjectid):
        fname  = "getTheClass"
        query  = "SELECT `id`,`teacher`,`subject`,`gradelevel`,`year` "
        query += "FROM `class` "
        query += "WHERE `id`='%s'"
        params = (subjectid)
        fullquery = query % params
        dbResult = self.db.executesql( fullquery )
        result = dbResult[0]
        #id, teacher, subject, gradeLevel, year
        thatClass = TheClass( result[0], result[1], result[2], result[3], result[4] )
        #return ( fullquery, thatClass )
        return thatClass
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getClasses(self):
        fname = "getClasses"
        query  = "SELECT `id`,`teacher`,`subject`,`gradelevel`,`year`"
        query += "FROM `class`"
        params = ()
        return self.db.executesql( query % params )
    #end getClasses
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+] participantID - identification for a participant
    # Returns: Grade info from the db for a teacher or researcher
    #**************************
    def getGrades(self, participantID):
        fname  = "getGrades"
        query  = "SELECT DISTINCT `c`.`gradelevel` "
        query += "FROM `class` as `c`, `participant` as `p`, "
        query += "`participanttype` as `pt` "
        query += "WHERE `c`.`teacher`=`p`.`id` and `p`.`id`='%s' "
        query += "AND ( `p`.`typeid`='teacher' OR `p`.`typeid`='researcher' ) "
        params = (participantID)
        return self.db.executesql( query % params )
    #end getGrades
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getVideos(self):
        fname = "getVideos"
        query  = "SELECT `location`,`type`,`description` "
        query += "FROM `video`"
        params = ()
        return self.db.executesql( query % params )
    #end getVideos
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getVideoTypes(self):
        fname  = "getVideoTypes"
        query  = "SELECT `type`,`description` "
        query += "FROM `videotype`"
        params = ()
        return self.db.executesql(query % params)
    #end getVideoTypes
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getSubjects(self, participantID):
        fname = "getSubjects"
        if self.debug:
            print "\nBegin [getSubjects]\n"
        query = "SELECT DISTINCT `c`.`subject`, `c`.`id`, ";
        query += "`c`.`teacher`, `c`.`gradelevel`, `c`.`year` ";
        query += "FROM `class` as `c`, `participant` as `p`, "
        query += "`participanttype` as `pt` "
        query += "WHERE `c`.`teacher`=`p`.`id` and `p`.`id`='%s' "
        query += "AND ( `p`.`typeid`='teacher' OR `p`.`typeid`='researcher' ) "
        params = (participantID)
        return self.db.executesql(query % params)
    #end getSubjects
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getSubjectsAtGradeLevel(self, participantID, gradeLevel):
        fname = "getSubjectsAtGradeLevel"
        if self.debug:
            print "\nBegin [getSubjectsAtGradeLevel]\n"
        query = "SELECT DISTINCT `c`.`subject`, `c`.`id`, ";
        query += "`c`.`teacher`, `c`.`gradelevel`, `c`.`year` ";
        query += "FROM `class` as `c`, `participant` as `p`, "
        query += "`participanttype` as `pt` "
        query += "WHERE `c`.`teacher`=`p`.`id` and `p`.`id`='%s' "
        query += "AND ( `p`.`typeid`='teacher' OR `p`.`typeid`='researcher' ) "
        query += "AND `c`.`gradelevel`='%s'"
        params = (participantID, gradeLevel)
        return ( (query % params), self.db.executesql(query % params) )
    #end getSubjects
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getGrades(self, participantID):
        fname  = "getGrades"
        query  = "SELECT DISTINCT `c`.`gradelevel` "
        query += "FROM `class` as `c`, `participant` as `p`, "
        query += "`participanttype` as `pt` "
        query += "WHERE `c`.`teacher`=`p`.`id` and `p`.`id`='%s' "
        query += "AND ( `p`.`typeid`='teacher' OR `p`.`typeid`='researcher' ) "
        params = (participantID)
        return self.db.executesql(query % params)
    #end getSubjects
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getStudents(self, classid, gradelevel, year, teacherid):
        fname = "getStudents"
        query  = "SELECT DISTINCT id, firstname, lastname, othernames, typeid "
        query += "FROM participant "
        query += "WHERE typeid='%s'"
        params = ('student')
        return self.db.executesql(query % params)
    #end getStudents
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getStudentsByClassAndDate(self, classid, theday):
        fname  = "getStudentsByClassAndDate"
        query  = "SELECT DISTINCT id, firstname, lastname, othernames, typeid "
        query += "FROM participant as p, participantclass as pc, "
        query += "participanteda as pe "
        query += "WHERE p.id = pc.participantid AND p.id = pe.participantid "
        query += "AND pe.beginday = '%s' "
        query += "AND pc.classid = '%s'"
        params = (theday, classid)
        return self.db.executesql(query % params)
    # end getStudentsByClassAndDate
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    def getDatesForClass(self,classid):
        fname =  "getDatesForClass"
        query =  "SELECT DISTINCT pe.beginday "
        query += "FROM participantengagement pe, participantclass pc "
        query += "WHERE pc.classid='%s' "
        query += "AND pe.participantid=pc.participantid "
        query += "ORDER BY 1 ASC "
        params = ( classid )
        return ( (query%params), self.db.executesql(query%params) )
    # end getDatesForClass

    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: queryString and queryResultSet
    #**************************
    #----------------------------------------------------------------#
    def getClassVideos(self, classid, theDate):
        fname  = "getClassVideos"
        query  = "SELECT DISTINCT location, view, begintimestamp, duration, "
        query += "participantid, sensorid, classid "
        query += "FROM participantclassentryvideo "
        query += "WHERE classid='%s' "
        query += "AND DATE(begintimestamp)='%s'"
        params = (classid, theDate)
        return ( (query%params), self.db.executesql(query%params) )
    #----------------------------------------------------------------#
    #**************************
    # Description: Pull engagement data for an entire class
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#
    def getEngagementLevelsFor(self, participantid, classid, beginDay):
        fname  = "getEngagementLevelsFor"
        query  = "SELECT DISTINCT pe.begintime, pe.level "
        query += "FROM participantengagement as pe, edaentry as ee "
        query += "WHERE pe.beginday=ee.beginday "
        query += "AND pe.participantid='%s' "
        query += "AND pe.beginday='%s' "
        query += "ORDER BY pe.begintime"
        params = ( participantid, beginDay )
        return ( (query%params), self.db.executesql(query%params) )
    # end getEngagementLevelsFor
    #----------------------------------------------------------------#
    #**************************
    # Description: Pull engagement data for an entire class
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#
    def getClassEngagementLevels(self, cid, theday):
        fname  = "getClassEngagementLevels"
        query  = "SELECT pe.participantid, level, begintime, duration "
        query += "FROM participantengagement pe, participantclass pc "
        query += "WHERE pe.participantid=pc.participantid "
        query += "AND pc.classid='%s' "
        query += "AND pe.beginday='%s' "
        query += "ORDER BY 1,3"
        params = (cid,theday)
        return ( (query%params), self.db.executesql(query%params) )
    # end getClassEngagementLevels
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#
    def getEdaPerMinute(self, participantid, classid, day):
        fname  = "getEdaPerMinute"
        query  = "SELECT DISTINCT edpm.timestamp, edpm.eda "
        query += "FROM edadataperminute as edpm, participanteda as pe, "
        query += "participantclass as pc, class as c, edaentry as ee "
        query += "WHERE edpm.sensorid=pe.sensorid "
        query += "AND pe.participantid=pc.participantid "
        query += "AND ee.sensorid=pe.sensorid "
        query += "AND ee.beginday=pe.beginday "
        query += "AND ee.begintime=pe.begintime "
        query += "AND pe.participantid='%s' "
        query += "AND c.id='%s' AND pe.beginday='%s' "
        query += "AND pe.location='left wrist' "
        query += "AND edpm.timestamp>=concat_ws(' ',ee.beginday,ee.begintime) "
        query += "AND edpm.timestamp<concat_ws(' ',ee.endday,ee.endtime) "
        query += "ORDER BY edpm.timestamp ASC "
        params = ( participantid, classid, day )
        #return self.db.executesql(query%params)
        return ( (query%params), self.db.executesql(query%params) )
    # end getReadingPerMinute
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#    
    def getEda(self, participantid, classid, day ):
        fname  = "getEda"
        query  = "SELECT DISTINCT edps.timestamp, edps.eda "
        query += "FROM edadatapersecond as edps, participanteda as pe, " 
        query += "participantclass as pc, class as c, edaentry as ee "
        query += "WHERE edps.sensorid=pe.sensorid "
        query += "AND pe.participantid=pc.participantid "
        query += "AND ee.sensorid=pe.sensorid AND ee.beginday=pe.beginday "
        query += "AND ee.begintime=pe.begintime "
        query += "AND pe.participantid='%s' "
        query += "AND c.id='%s' "
        query += "AND pe.beginday='%s' "
        query += "AND pe.location='left wrist' "
        query += "AND edps.timestamp>=concat_ws(' ',ee.beginday,ee.begintime) "
        query += "AND edps.timestamp<concat_ws(' ',ee.endday,ee.endtime) "
        query += "ORDER BY edps.timestamp ASC "
        params = ( participantid, classid, day )
        return ( (query % params ), self.db.executesql( query % params ) )
    # end getEda
    #----------------------------------------------------------------#
    #**************************
    # Description:
    # Parameters:
    # [+]  -
    # Returns: nothing
    #**************************
    #----------------------------------------------------------------#   
