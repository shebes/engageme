# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

import time
from datetime import date
from model.myengagement import MyEngagement

myPedometer    = DBGetters()
engagementLvls = []
#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    if not session.counter:
        session.counter = 1
    else:
        session.counter += 1

    session.user_exists = doesUserExist(request.vars.user_name)
    if session.user_exists:
        session.user_name = request.vars.user_name
        session.teacherid = request.vars.user_name
        #session.teacher   = myPedometer.getParticipant(request.vars.user_name)
        redirect(URL('load'))
    else:
        session.user_name = "Darkseid"

    #tableInfo = SQLTABLE(edb().select(edb.participant.ALL))
    response.flash = T("Welcome to EngageME!")
    return dict(message=T('Shelby did this'),counter=session.counter)

def d3test():
	return dict()
	
def edatoolkit():
	return dict()
	
def dataentry():
	return dict()

def doesUserExist(userName):
    # query DB for ParticipantID
    query = "SELECT * from participant WHERE id='%s'"
    existenceResult = mydb.getDB().executesql( query % userName )
    return existenceResult

def load():
    """
    There's a lot of work to do, no telling what's next
    """
    # I need the k
    gradeOptions  = "<option>10</option>"
    gradeOptions += "<option>11</option>"
    gradeOptions += "<option>12</option>"
    response.flash = T("Please select options to load a class.")
    userExists = session.user_exists
    return dict(userExists=userExists,gradeOptions=gradeOptions)

def showSubjects():
    # this function will query the db and show info on the load page
    # right now we've just implemented some simple ajax stuff
    session.grade = request.vars.gradeSelect
    classSubjects = myPedometer.getSubjectsAtGradeLevel(session.teacherid, session.grade)
    subjectList = classSubjects[1]
    output  = "<label for=\"selectSubject\" class=\"col-lg-2 control-label\">Subject</label>"
    output += "<div class=\"col-lg-10\">"
    output += "<select class=\"form-control\" name=\"selectSubject\" id=\"selectSubject\" "
    output += "onchange=\"ajax('showDates',['selectSubject'],'loadDateDiv')\">"
    output += "<option value=\"\">Select a subject...</option>"
    for subject in subjectList:
        output += "<option value=\"" + subject[1] + "\">" 
        output += subject[1] + " " + subject[0] + "</option>"
    output += "</select>"
    output += "</div>"
    return output

"""
*************************************************************************
* Figuring out how to make this work was interesting because of the
* type returned by the database.  I think it is a date object of some
* sort, but I don't know.  I had to stringify the object before
* concatenating it with other strings.
*************************************************************************
"""
def showDates():
    session.selectedSubject = request.vars.selectSubject
    queryResult = myPedometer.getDatesForClass(session.selectedSubject)
    myQuery     = queryResult[0]
    classDates  = queryResult[1]
    output  = "<label for=\"selectDate\" class=\"col-lg-2 control-label\">Date</label>"
    output += "<div class=\"col-lg-10\">"
    output += "<select class=\"form-control\" name=\"selectDate\" id=\"selectDate\" "
    output += "onchange=\"ajax('setClassDate',['selectDate'],'emptyDiv')\">"
    output += "<option value=\"\">Select a date...</option>"
    for myDate in classDates:
        output += "<option value=\"" + myDate[0].__str__() + "\">" 
        output += myDate[0].__str__() + "</option>"
    output += "</select>"
    output += "</div>"
    return output

def setClassDate():
    session.classDate = request.vars.selectDate
    return "" #"session.classDate has been set to " + session.classDate

################
# This function sets up the EDA graph for a single student
################
def setupIndividualInformation():
	_pid       = session.currentStudentID
	_class_id  = session.selectedSubject
	_class_day = session.classDate
	_eda       = myPedometer.getEda( _pid, _class_id, _class_day )
	result     = "<script>"
	result    += "\n"
	result    += "var data = ["
	result    += "\n"
	if ( len(_eda) == 2 ):
		ndx = 0
		eda_data = _eda[1]
		for entry in eda_data:#[0:500]:
			if ndx > 0:
				result += ",\n"			
			result += "["
			result += "Date.parse('%s'), %f" % (entry[0], entry[1])
			result += "]"
			ndx += 1
		# end for loop
	result     += "];"
	result    += "\n"
	result    += "// make the container smaller and add a second container for the master chart"
	result    += "\n"
	result    += "var $container = $('#chart').css('position', 'relative');"
	result    += "\n"
	result    += "var $detailContainer = $('<div id=\"detail-container\" class=\"span12\">').appendTo($container);"
	result    += "\n"
	result    += "$(function() { createDetail(data); });"
	result    += "\n"
	result     += "</script>"
	return result
# end setupIndividualInformation

def setFullStudentName():
	setCurrentStudent(request.vars.studentID)
	# this is a Participant object
	myParticipant = myPedometer.getParticipant(session.currentStudentID)
	return "%s" % ( myParticipant.getFullName() )
# end setFullStudentName

def setCurrentStudent(value):
	session.currentStudentID = value
# end setCurrentStudent

################################################################################
# Description: This function 
# Parameters:
# [+] none
# Return: Nothing
################################################################################
def group():
    """
    There's a lot of work to do, no telling what's next.
    I have to get the engagement levels for each student in the class.
    """
    engagementLvls = myPedometer.getClassEngagementLevels(session.selectedSubject, session.classDate)
    query          = engagementLvls[0]
    engagementLvls = engagementLvls[1]
    (eResult,classNgmnt) = formatClassEngagementLevels(engagementLvls)
    response.flash = T("View the interest levels of the students in your class.")
    return dict(message=T(''),query=query,classNgmnt=classNgmnt,eLevels=eResult)
# end group
#------------------------------------------------------------------------------

################################################################################
# Description: This function
# Parameters:
# [+] none
# Return: Nothing
################################################################################
def formatClassEngagementLevels( ngmnt_lvls ):
	fName   = 'formatClassEngagementLevels'
	output  = ''
	lastPid = ''
	ndx = 0
	classNgmnt = []
	for entry in ngmnt_lvls:
		pid   = entry[0]
		level = entry[1]
		bDT   = entry[2]
		step  = entry[3]
		if (ndx == 0):
			lastPid = pid
			singleEngagement = MyEngagement(pid,bDT,step)
			singleEngagement.addLevel(level)
		else:
			if ( lastPid == pid ):
				singleEngagement.addLevel(level)
			else:
				output += '%s' % singleEngagement
				classNgmnt.append( singleEngagement )
				lastPid = pid
				singleEngagement = MyEngagement(pid,bDT,step)
				singleEngagement.addLevel(level)
		ndx += 1
	classNgmnt.append( singleEngagement )
	output += '%s' % singleEngagement
	return (output,classNgmnt)
# end formatClassEngagementLevels
#------------------------------------------------------------------------------

################################################################################
# Description: This function 
# Parameters:
# [+] none
# Return: Nothing
################################################################################
def populateStudentArray():
    classStudents = myPedometer.getStudentsByClassAndDate(session.selectedSubject, session.classDate)
    output  = "<script>"
    output += "\n\t"
    """
    output += "var deskWidth  = 60;"
    output += "\n\t"
    output += "var deskHeight = 45;"
    output += "\n\t"
    output += "var numStudents = {{=numStudents}};"
    output += "\n\t"
    output += "var numVideos   = {{=numVideos}};"
    output += "\n\t"
    output += "var studentName = 'Boo Shanks';"
    output += "\n\t"
    """
    output += "var students = new Array();"
    output += "\n\t"
    for student in classStudents:
    	tmpout  = "students.push('%s');"
    	params  = myPedometer.getParticipant(student[0]).getFullName()
        output += tmpout % params
    output += "</script>"
    return output

    
def showStudent():
	
	output=""
	return output

def video():
    return dict()

def test():
    return dict()

def carousel():
    return dict()
    
def lines():
	return dict()
    
def extensions():
	return dict()

def echo():
    return jQuery('#student30').html('%s' % repr(request.vars.name))

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())


def handleGradeDrpDwn():
    response.flash = T("Value in dropdown has changed.")
# end handleGradeDrpDwn